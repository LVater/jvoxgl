package de.lv.jvoxgl.utilities;

import de.lv.jvoxgl.world.World;

public class Coordinates {
	
	/**
	 * Converts x-world-coordinates to x-chunk-coordinates
	 * @param world
	 * @param wx
	 * @return cx
	 */
	public static int WorldToChunkCoordinatesX(World world, int wx){
		int cw = world.getChunkWidth();
		int cx = (int)Math.floor((float)wx/(float)cw);
		return cw-(cw*(cx+1)-wx);
	}
	
	/**
	 * Converts z-world-coordinates to z-chunk-coordinates
	 * @param world
	 * @param wz
	 * @return cz
	 */
	public static int WorldToChunkCoordinatesZ(World world, int wz){
		int cd = world.getChunkDepth();
		int cz = (int)Math.floor((float)wz/(float)cd);
		return cd-(cd*(cz+1)-wz);
	}
}
