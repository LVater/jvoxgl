package de.lv.jvoxgl.utilities;

public class Log {
	
	public static final int LEVEL_NONE=-1;
	public static final int LEVEL_ERROR=0;
	public static final int LEVEL_WARNING=1;
	public static final int LEVEL_INFO=2;
	public static final int LEVEL_DEBUG=3;
	
	private static int LOG_LEVEL = Log.LEVEL_ERROR;
	
	/**
	 * Returns the current Log Level
	 * @return LOG_LEVEL
	 */
	public static int getLogLevel(){
		return Log.LOG_LEVEL;
	}
	
	/**
	 * Sets the current log level
	 * @param level
	 */
	public static void setLogLevel(int level){
		Log.LOG_LEVEL = level;
	}
	
	/**
	 * Logs an error
	 * @param msg
	 */
	public static void error(String msg){
		if(LOG_LEVEL >= LEVEL_ERROR)System.out.println("[ERROR] "+msg);
	}
	
	
	/**
	 * Logs a warning
	 * @param msg
	 */
	public static void warning(String msg){
		if(LOG_LEVEL >= LEVEL_WARNING)System.out.println("[WARNING] "+msg);
	}
	
	/**
	 * Logs an info
	 * @param msg
	 */
	public static void info(String msg){
		if(LOG_LEVEL >= LEVEL_INFO)System.out.println("[INFO] "+msg);
	}
	
	/**
	 * Logs a debug message
	 * @param msg
	 */
	public static void debug(String msg){
		if(LOG_LEVEL >= LEVEL_DEBUG)System.out.println("[DEBUG] "+msg);
	}
	
}
