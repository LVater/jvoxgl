package de.lv.jvoxgl.tests;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Random;

import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import de.lv.jvoxgl.JVoxGL;
import de.lv.jvoxgl.render.ShaderProgram;
import de.lv.jvoxgl.utilities.Log;
import de.lv.jvoxgl.world.World;
import de.lv.jvoxgl.world.entities.Block;

public class JVoxGLTest {
	
	public static FPSCameraController cam;
	public static World world;
	
	public static void main(String[] args){
		Log.setLogLevel(Log.LEVEL_DEBUG);
		
		world = new World(16, 256, 16);
		JVoxGL voxgl = new JVoxGL(world, 1024,768);
		JVoxGLTest.createChunk(world);
		
		ShaderProgram shader = new ShaderProgram(2);
		try {
			shader.loadShader(0, GL20.GL_VERTEX_SHADER, new File("vertex.glsl"));
			shader.loadShader(1, GL20.GL_FRAGMENT_SHADER, new File("fragment.glsl"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		HashMap<Integer, String> att = new HashMap<Integer, String>();
		att.put(0, "in_Position");
		shader.create(att);
		
		world.setShader(shader);
		
		cam = new FPSCameraController(0, -30, -40);
		Mouse.setGrabbed(true);
		
		GL11.glTranslatef(0, -30, -40);
		while(!Display.isCloseRequested() && !Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)){
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
			
			JVoxGLTest.updateCam();
			
			voxgl.render();
			
			Display.sync(60);
			Display.update();
		}
		
		voxgl.destroy();
		Display.destroy();
	}
	
	private static float dx,dy,dt,lastTime,time,mousesens=0.5f,walkspeed=40.0f;
	
	public static void updateCam(){
		time = Sys.getTime();
		dt = (time-lastTime)/1000.0f;
		lastTime = time;
		
		dx = Mouse.getDX();
		dy = Mouse.getDY();
		
		cam.yaw(dx*mousesens);
		cam.pitch(dy*mousesens);
		
		GL11.glLoadIdentity();
		cam.lookThrough();
		
		float sprint = 0.0f;
		if(Keyboard.isKeyDown(Keyboard.KEY_LSHIFT))sprint=5f;
		
		if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)){
			cam.flyUp(walkspeed*dt+sprint);
		}
		
		if(Keyboard.isKeyDown(Keyboard.KEY_LCONTROL)){
			cam.flyDown(walkspeed*dt+sprint);
		}
		
		if(Keyboard.isKeyDown(Keyboard.KEY_W)){
			cam.walkForward(walkspeed*dt+sprint);
		}
		
		if(Keyboard.isKeyDown(Keyboard.KEY_S)){
			cam.walkBackwards(walkspeed*dt+sprint);
		}
		
		if(Keyboard.isKeyDown(Keyboard.KEY_A)){
			cam.strafeLeft(walkspeed*dt+sprint);
		}
		
		if(Keyboard.isKeyDown(Keyboard.KEY_D)){
			cam.strafeRight(walkspeed*dt+sprint);
		}
		
	}
	
	public static void createChunk(World world){
		Random rand = new Random(System.currentTimeMillis());
		for(int x=0;x<(world.getChunkWidth()*5);x++){
			for(int y=0;y<world.getChunkHeight();y++){
				for(int z=0;z<(world.getChunkDepth()*5);z++){
					if(rand.nextInt(20)==0){
						new Block(world, 1, x, y, z);
					}
				}
			}
		}
	}
	
}
