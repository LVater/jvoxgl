package de.lv.jvoxgl;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;

import de.lv.jvoxgl.world.Chunk;
import de.lv.jvoxgl.world.World;

public class JVoxGL {
	
	private int width,height;
	private World world;
	
	/**
	 * Creates a JVoxGL instance with an existing Display.
	 * @throws JVoxGLException
	 */
	public JVoxGL(World world) throws JVoxGLException{
		if(!Display.isCreated())throw new JVoxGLException();
		this.width = Display.getWidth();
		this.height = Display.getHeight();
		this.setWorld(world);
		this.initGL();
	}
	
	/**
	 * Create a JVoxGL instance with specified width and height(Also creates the Display)
	 * @param width
	 * @param height
	 */
	public JVoxGL(World world, int width, int height){
		this.width = width;
		this.height = height;
		this.setWorld(world);
		
		try {
			Display.setDisplayMode(new DisplayMode(width,height));
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		this.initGL();
	}
	
	/**
	 * Initialize needed OpenGL
	 */
	private void initGL(){
		
		//Sets up the Viewport
		GL11.glViewport(0, 0, this.width, this.height);
		
		//Enable needed Flags
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glEnable(GL11.GL_CULL_FACE);
		
		//Set up Matrix and Perspective
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GLU.gluPerspective(45, (float)width/(float)height, 0.3f, 8000);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
	}
	
	/**
	 * Renders the Scene
	 */
	public void render(){
		
		//Render Chunks
		for(Chunk chunk: world.getChunks()){
			if(chunk.isChanged())chunk.updateChunk();
			chunk.getRenderer().render();
		}
	}
	
	/**
	 * Destroys the Scene
	 */
	public void destroy(){
		for(Chunk chunk: world.getChunks()){
			chunk.getRenderer().destroy();
		}
	}

	/**
	 * Returns the display width
	 * @return width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Returns the display height
	 * @return height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * @return world
	 */
	public World getWorld() {
		return world;
	}

	/**
	 * @param world
	 */
	public void setWorld(World world) {
		this.world = world;
	}

}
