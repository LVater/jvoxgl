package de.lv.jvoxgl.render;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import de.lv.jvoxgl.utilities.Log;

public class ShaderProgram {
	
	private int[] shaders;
	private int program;
	
	public ShaderProgram(int shadernum){
		this.setShaders(new int[shadernum]);
	}
	
	/**
	 * Loads a Shader from File
	 * @param id
	 * @param type
	 * @param file
	 * @throws IOException
	 */
	public void loadShader(int id, int type, File file) throws IOException{
		StringBuilder shaderSource = new StringBuilder();
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line = "";
		while((line=reader.readLine()) != null){
			shaderSource.append(line).append("\n");
		}
		reader.close();
		
		int shaderId = GL20.glCreateShader(type);
		GL20.glShaderSource(shaderId, shaderSource);
		GL20.glCompileShader(shaderId);
		if(GL20.glGetShaderi(shaderId, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE){
			Log.error("Shader compile error");
			System.exit(-1);
		}
		shaders[id]=shaderId;
	}
	
	/**
	 * Creates the Program
	 * @param attributes
	 */
	public void create(HashMap<Integer,String> attributes){
		this.program = GL20.glCreateProgram();
		for(int i=0;i<this.shaders.length;i++){
			GL20.glAttachShader(program, shaders[i]);
		}
		GL20.glLinkProgram(program);
		
		for(Integer aid: attributes.keySet()){
			GL20.glBindAttribLocation(program, aid, attributes.get(aid));
		}
		
		GL20.glValidateProgram(program);
	}
	
	/**
	 * Binds the Shader-Program
	 */
	public void bind(){
		GL20.glUseProgram(program);
	}
	
	/**
	 * Unbinds the Shader-Program
	 */
	public void unbind(){
		GL20.glUseProgram(0);
	}

	/**
	 * @return shaders
	 */
	public int[] getShaders() {
		return shaders;
	}

	/**
	 * @param shaders
	 */
	public void setShaders(int[] shaders) {
		this.shaders = shaders;
	}
}
