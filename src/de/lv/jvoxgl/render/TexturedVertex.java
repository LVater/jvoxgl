package de.lv.jvoxgl.render;

public class TexturedVertex {
	private int x,y,z,id;
	
	/**
	 * Creates a Textured Vertex
	 * @param x
	 * @param y
	 * @param z
	 * @param id
	 */
	public TexturedVertex(int x, int y, int z, int id){
		this.x = x;
		this.y = y;
		this.z = z;
		this.id = id;
	}

	/**
	 * Returns the X-Position
	 * @return x
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Sets the X-Position
	 * @param x
	 */
	public void setX(int x) {
		this.x = x;
	}
	
	/**
	 * Returns the Y-Position
	 * @return y
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * Sets the Y-Position
	 * @param y
	 */
	public void setY(int y) {
		this.y = y;
	}
	
	/**
	 * Returns the Z-Position
	 * @return z
	 */
	public int getZ() {
		return z;
	}
	
	/**
	 * Sets the Z-Position
	 * @param z
	 */
	public void setZ(int z) {
		this.z = z;
	}
	
	/**
	 * Returns the ID
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the ID
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	
}
