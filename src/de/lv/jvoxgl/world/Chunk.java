package de.lv.jvoxgl.world;

import de.lv.jvoxgl.render.Renderer;
import de.lv.jvoxgl.world.entities.Block;

public class Chunk {
	private int x,z;
	private Block[][][] blocks;
	private World world;
	private Renderer renderer;
	private boolean changed;
	
	/**
	 * Creates a Chunk inside a World
	 * @param world
	 * @param x
	 * @param z
	 */
	public Chunk(World world, int x, int z){
		this.x = x;
		this.z = z;
		this.blocks = new Block[world.getChunkWidth()][world.getChunkHeight()][world.getChunkDepth()];
		this.world = world;
		this.renderer = new Renderer(world.getShader());
		renderer.setSize(10);
		world.getChunks().add(this);
	}
	
	/**
	 * Adds a Block to this Chunk
	 * @param block
	 * @param x
	 * @param y
	 * @param z
	 */
	public void addBlock(Block block, int x, int y, int z){
		if(x < 0 || x > world.getChunkWidth()-1 || y < 0 || y > world.getChunkHeight()-1 || z < 0 || z > world.getChunkDepth()-1)return;
		blocks[x][y][z] = block;
		this.setChanged(true);
	}
	
	/**
	 * Updates the Chunk
	 */
	public void updateChunk(){
		for(int cx=0;cx<world.getChunkWidth();cx++){
			for(int cy=0;cy<world.getChunkHeight();cy++){
				for(int cz=0;cz<world.getChunkDepth();cz++){
					Block block = blocks[cx][cy][cz];
					if(block == null)continue;
					if(block.getId() == 0)continue;
					this.renderer.addVertices(block.getVertices());
				}
			}
		}
		this.renderer.fill();
		this.setChanged(false);
	}
	
	/**
	 * Returns the X-Position
	 * @return x
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Sets the X-Position
	 * @param x
	 */
	public void setX(int x) {
		this.x = x;
	}
	
	/**
	 * Returns the Z-Position
	 * @return z
	 */
	public int getZ() {
		return z;
	}
	
	/**
	 * Sets the Z-Position
	 * @param z
	 */
	public void setZ(int z) {
		this.z = z;
	}

	/**
	 * @return blocks
	 */
	public Block[][][] getBlocks() {
		return blocks;
	}

	/**
	 * @param blocks
	 */
	public void setBlocks(Block[][][] blocks) {
		this.blocks = blocks;
	}

	/**
	 * @return world
	 */
	public World getWorld() {
		return world;
	}

	/**
	 * @param world
	 */
	public void setWorld(World world) {
		this.world = world;
	}

	/**
	 * @return renderer
	 */
	public Renderer getRenderer() {
		return renderer;
	}

	/**
	 * @param renderer
	 */
	public void setRenderer(Renderer renderer) {
		this.renderer = renderer;
	}

	/**
	 * @return the changed
	 */
	public boolean isChanged() {
		return changed;
	}

	/**
	 * @param changed the changed to set
	 */
	public void setChanged(boolean changed) {
		this.changed = changed;
	}
	
}
