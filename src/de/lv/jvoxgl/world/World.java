package de.lv.jvoxgl.world;

import java.util.ArrayList;
import java.util.List;

import de.lv.jvoxgl.render.ShaderProgram;
import de.lv.jvoxgl.utilities.Coordinates;
import de.lv.jvoxgl.world.entities.Block;

public class World {
	private List<Chunk> chunks = new ArrayList<Chunk>();
	private int chunk_width,chunk_height,chunk_depth;
	private ShaderProgram shader;
	
	/**
	 * Creates a World
	 * @param chunk_w
	 * @param chunk_h
	 * @param chunk_d
	 */
	public World(int chunk_w, int chunk_h, int chunk_d){
		this.chunk_width = chunk_w;
		this.chunk_height = chunk_h;
		this.chunk_depth = chunk_d;
	}
	
	/**
	 * Returns a list of all chunks
	 * @return chunks
	 */
	public List<Chunk> getChunks(){
		return this.chunks;
	}
	
	/**
	 * Gets the chunk at specified chunk-coordinates
	 * @param x
	 * @param z
	 * @return chunk
	 */
	public Chunk getChunk(int x, int z){
		for(Chunk chunk: chunks){
			if(chunk.getX() == x && chunk.getZ() == z)return chunk;
		}
		return null;
	}
	
	/**
	 * Gets the chunk at specified world-coordinates
	 * @param x
	 * @param z
	 * @return chunk
	 */
	public Chunk getChunkAt(int x, int z){
		int cx = (int) Math.floor((float)x/(float)this.getChunkWidth());
		int cz = (int) Math.floor((float)z/(float)this.getChunkDepth());
		return this.getChunk(cx, cz);
	}
	
	/**
	 * Sets the block id at a specified point
	 * @param id
	 * @param x
	 * @param y
	 * @param z
	 */
	public void setBlockIdAt(int id, int x, int y, int z){
		Block block = this.getBlockAt(x, y, z);
		if(block == null){
			new Block(this, id, x, y, z);
		}else{
			block.setId(id);
		}
	}
	
	/**
	 * Gets a Block
	 * @param x
	 * @param y
	 * @param z
	 * @return block
	 */
	public Block getBlockAt(int x, int y, int z){
		Chunk chunk = this.getChunkAt(x, z);
		if(chunk == null)return null;
		int cx = Coordinates.WorldToChunkCoordinatesX(this, x);
		int cz = Coordinates.WorldToChunkCoordinatesZ(this, z);
		if(cx < 0 || cx > chunk_width-1 || cz < 0 || cz > chunk_depth-1 || y < 0 || y > chunk_height-1)return null;
		Block block = chunk.getBlocks()[cx][y][cz];
		return block;
	}
	
	/**
	 * 
	 * @return chunk_width
	 */
	public int getChunkWidth() {
		return chunk_width;
	}
	
	/**
	 * 
	 * @param chunk_width
	 */
	public void setChunkWidth(int chunk_width) {
		this.chunk_width = chunk_width;
	}

	/**
	 * 
	 * @return chunk_height
	 */
	public int getChunkHeight() {
		return chunk_height;
	}

	/**
	 * 
	 * @param chunk_height
	 */
	public void setChunkHeight(int chunk_height) {
		this.chunk_height = chunk_height;
	}

	/**
	 * 
	 * @return chunk_depth
	 */
	public int getChunkDepth() {
		return chunk_depth;
	}

	/**
	 * 
	 * @param chunk_depth
	 */
	public void setChunkDepth(int chunk_depth) {
		this.chunk_depth = chunk_depth;
	}

	/**
	 * @return the shader
	 */
	public ShaderProgram getShader() {
		return shader;
	}

	/**
	 * @param shader the shader to set
	 */
	public void setShader(ShaderProgram shader) {
		for(Chunk chunk: this.chunks){
			chunk.getRenderer().setShader(shader);
		}
		this.shader = shader;
	}
}
