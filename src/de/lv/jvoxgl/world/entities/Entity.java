package de.lv.jvoxgl.world.entities;

import java.util.List;

import de.lv.jvoxgl.render.TexturedVertex;
import de.lv.jvoxgl.world.World;

public abstract class Entity {
	
	protected World world;
	protected int x,y,z;
	
	/**
	 * Creates the Entity inside the world
	 * @param world
	 */
	public Entity(World world, int x, int y, int z){
		this.world = world;
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	/**
	 * Returns a List of Textured Vertices
	 * @return vertices
	 */
	public abstract List<TexturedVertex> getVertices();
	
	/**
	 * Returns the X-Position
	 * @return x
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Sets the X-Position
	 * @param x
	 */
	public void setX(int x) {
		this.x = x;
	}
	
	/**
	 * Returns the Y-Position
	 * @return y
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * Sets the Y-Position
	 * @param y
	 */
	public void setY(int y) {
		this.y = y;
	}
	
	/**
	 * Returns the Z-Position
	 * @return y
	 */
	public int getZ() {
		return z;
	}
	
	/**
	 * Sets the Z-Position
	 * @param z
	 */
	public void setZ(int z) {
		this.z = z;
	}

	/**
	 * Returns the World
	 * @return world
	 */
	public World getWorld(){
		return this.world;
	}
}
