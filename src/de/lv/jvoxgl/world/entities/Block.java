package de.lv.jvoxgl.world.entities;

import java.util.ArrayList;
import java.util.List;

import de.lv.jvoxgl.render.TexturedVertex;
import de.lv.jvoxgl.utilities.Coordinates;
import de.lv.jvoxgl.world.Chunk;
import de.lv.jvoxgl.world.World;

public class Block extends Entity {
	
	private int id;
	
	public Block(World world, int id, int x, int y, int z) {
		super(world,x,y,z);
		this.setId(id);
		
		Chunk chunk = world.getChunkAt(x, z);
		int cx = (int) Math.floor((float)x/(float)world.getChunkWidth());
		int cz = (int) Math.floor((float)z/(float)world.getChunkDepth());
		if(chunk == null){
			chunk = new Chunk(world, cx, cz);
		}
		if(world.getBlockAt(x, y, z) == null)chunk.addBlock(this, Coordinates.WorldToChunkCoordinatesX(world, x), y, Coordinates.WorldToChunkCoordinatesZ(world, z));
	}

	@Override
	public List<TexturedVertex> getVertices() {
		List<TexturedVertex> vertices = new ArrayList<TexturedVertex>();
		if(world.getBlockAt(x, y, z+1) == null)this.addFrontVertices(vertices);
		if(world.getBlockAt(x, y, z-1) == null)this.addBackVertices(vertices);
		if(world.getBlockAt(x-1, y, z) == null)this.addLeftVertices(vertices);
		if(world.getBlockAt(x+1, y, z) == null)this.addRightVertices(vertices);
		if(world.getBlockAt(x, y+1, z) == null)this.addTopVertices(vertices);
		if(world.getBlockAt(x, y-1, z) == null)this.addBottomVertices(vertices);
		return vertices;
	}
	
	/**
	 * Adds the Front-Face Vertices
	 * @param vertices
	 */
	private void addFrontVertices(List<TexturedVertex> vertices){
		vertices.add(new TexturedVertex(x, y, z, id));
		vertices.add(new TexturedVertex(x, y-1, z, id));
		vertices.add(new TexturedVertex(x+1, y-1, z, id));
		//vertices.add(new TexturedVertex(x+1, y-1, z, id));
		vertices.add(new TexturedVertex(x+1, y, z, id));
		//vertices.add(new TexturedVertex(x, y, z, id));
	}
	
	/**
	 * Adds the Back-Face Vertices
	 * @param vertices
	 */
	private void addBackVertices(List<TexturedVertex> vertices){
		vertices.add(new TexturedVertex(x, y, z-1, id));
		vertices.add(new TexturedVertex(x+1, y, z-1, id));
		vertices.add(new TexturedVertex(x+1, y-1, z-1, id));
		//vertices.add(new TexturedVertex(x+1, y-1, z-1, id));
		vertices.add(new TexturedVertex(x, y-1, z-1, id));
		//vertices.add(new TexturedVertex(x, y, z-1, id));
	}
	
	/**
	 * Adds the Left-Face Vertices
	 * @param vertices
	 */
	private void addLeftVertices(List<TexturedVertex> vertices){
		vertices.add(new TexturedVertex(x, y, z-1, id));
		vertices.add(new TexturedVertex(x, y-1, z-1, id));
		vertices.add(new TexturedVertex(x, y-1, z, id));
		//vertices.add(new TexturedVertex(x, y-1, z, id));
		vertices.add(new TexturedVertex(x, y, z, id));
		//vertices.add(new TexturedVertex(x, y, z-1, id));
	}
	
	/**
	 * Adds the Right-Face Vertices
	 * @param vertices
	 */
	private void addRightVertices(List<TexturedVertex> vertices){
		vertices.add(new TexturedVertex(x+1, y, z-1, id));
		vertices.add(new TexturedVertex(x+1, y, z, id));
		vertices.add(new TexturedVertex(x+1, y-1, z, id));
		//vertices.add(new TexturedVertex(x+1, y-1, z, id));
		vertices.add(new TexturedVertex(x+1, y-1, z-1, id));
		//vertices.add(new TexturedVertex(x+1, y, z-1, id));
	}
	
	/**
	 * Adds the Top-Face Vertices
	 * @param vertices
	 */
	private void addTopVertices(List<TexturedVertex> vertices){
		vertices.add(new TexturedVertex(x, y, z, id));
		vertices.add(new TexturedVertex(x+1, y, z, id));
		vertices.add(new TexturedVertex(x+1, y, z-1, id));
		//vertices.add(new TexturedVertex(x+1, y, z-1, id));
		vertices.add(new TexturedVertex(x, y, z-1, id));
		//vertices.add(new TexturedVertex(x, y, z, id));
	}
	
	/**
	 * Adds the Bottom-Face Vertices
	 * @param vertices
	 */
	private void addBottomVertices(List<TexturedVertex> vertices){
		vertices.add(new TexturedVertex(x, y-1, z, id));
		vertices.add(new TexturedVertex(x, y-1, z-1, id));
		vertices.add(new TexturedVertex(x+1, y-1, z-1, id));
		//vertices.add(new TexturedVertex(x+1, y-1, z-1, id));
		vertices.add(new TexturedVertex(x+1, y-1, z, id));
		//vertices.add(new TexturedVertex(x, y-1, z, id));
	}
	
	/** Returns the ID
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/** Sets the ID
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

}
